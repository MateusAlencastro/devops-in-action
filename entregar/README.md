# Automatizar o pipeline para após o sucesso da build executar o deploy e a atualização do servidor de produção

## Setup

Para incluirmos a etapa de deploy integrado com o processo de build no servidor de integração contínua vamos precisar colocar os dois servidores na mesma rede, para isso vamos executar os comandos abaixo:

> sudo docker network create devopsnet

> sudo docker network connect devopsnet cpf-server

> sudo docker network connect devopsnet cpf-api-ci

Agora precisamos autorizar o acesso SSH do servidor Jenkins ao servidor onde está a aplicação, vamos executar os passos a seguir:

1.  Acessar o servidor Jenkins para gerar a chave SSH

> sudo docker exec -it cpf-api-ci /bin/bash

2.  Execute o comando para gerar a chave, confirme todas as etapas e não precisa especificar nada, deixe todos os valores no padrão, e não inclua nenhuma senha também.

> ssh-keygen -t rsa

3.  Após a execução do comando vamos copiar a chave SSH gerada para poder incluir no servidor da aplicação:

> cat ~/.ssh/id_rsa.pub

4.  Vamos adicionar a chave do servidor CI no servidor da aplicação, primeiro vamos acessar o servidor via SSH com o comando abaixo:

> ssh root@devops.prod

5.  Edite o arquivo abaixo e cole a chave SSH que você copiou do servidor Jenkins anteriormente:

> vim ~/.ssh/authorized_keys

Ao final desse passos o servidor Jenkins poderá acessar o servidor da aplicação via SSH sem solicitar usuário e senha. Isso é o suficiente para executarmos o playbook usando Ansible e configurar realizar o deploy da aplicação.

Antes de passar para o exercício vamos testar a conexão a partir do servidor Jenkins

> sudo docker exec -it cpf-api-ci /bin/bash

> ssh root@cpf-server

Se tudo der certo estamos prontos para os próximos passos.

## Exercício

Vamos integrar os dois processos o de Build e o de Deploy, disparando o processo de deploy sempre que uma build nova e estável for concluída. Os passos seguintes irão criar um novo Job no Jenkins integrado com o Job anterior que realiza o build da aplicação:

1.  Criar projeto Freestyle com o nome `deploy_cpf_api`

2.  Adicionar o repositório devops-in-action `https://gitlab.com/pigor/devops-in-action.git` na sessão `Source Code Management`.

3.  Na sessão `Build Triggers` marque a opção `Build after other projects are built` e coloque o nome do Job do processo de deploy.

4.  Na sessão `Build` adicione um `Execute Shell` com o comando de execução do playbook

```
ansible-playbook -i entregar/hosts entregar/app.yml
```

5.  Pronto agora o processo de deploy está integrado ao processo de build e sempre que o processo de build for concluído com sucesso, o processo de deploy é iniciado automaticamente atualizando o servidor com a nova versão da aplicação.

Vamos validar, execute o `Build Now` do Job de build da aplicação e vamos observar ao finalizar o processo se o processo de deploy é iniciado automaticamente.

6. Para testar se a aplicação ficou disponível acesse `http://app.devops.prod/`
